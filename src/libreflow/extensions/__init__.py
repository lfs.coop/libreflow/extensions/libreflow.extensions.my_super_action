
from kabaret import flow
from libreflow.utils.flow.extension_manager import classqualname



class MySuperAction(flow.Action):
    pass
    

def get_handlers():
    return [
        ("^/[^/]+/films/[^/]+$", "my_super_action", classqualname(MySuperAction), {'label': 'My grEat action', 'custom_page': 'libreflow.extensions.ui.my_great_action.MyGreatActionWidget'})
    ]
