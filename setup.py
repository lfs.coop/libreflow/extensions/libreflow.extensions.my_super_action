import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...

setup(
    name = "my_super_action",
    version = "0.4dev",
    author = "LFS Flavio Perez",
    author_email = "flavio@lfscoop",
    description = ("Fake example action."),
    license = "GPL",
    keywords = "example documentation tutorial",
    package_dir={"": "src"},
    packages=['libreflow.extensions'],
    long_description="TODO",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)